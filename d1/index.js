//NODE JS INTRODUCTION

//use t5he require directive to load node.js module.
// a module is a software component or part of a program that contains one or more routines.
// http module lets node.js transfer data using the hyper text transfer protocol 
//http is a set of indi8vidual files that contaisn code to create component that help establish data transfer between application
//HTTP as a protocol that allow the fetching of resources such as HTML documents
//client (browser) and servers (node.js/expres.js application) communicate by exchanging individual message 
//the message sent by the client, usually, a web browser, are called request
// the messages sent by the server as an answer are called response

let http = require("http");


//the http module has a createServer method that accepts a function as an argument and allows for a creation of a server
//the arguments passed in the function are request and response


http.createServer(function (request, response) {

	response.writeHead(200, {'Content-Type': 'text/plain'})
	response.end('Hello World');

}).listen(4000)


//a port is a vertual point where the  networt connection starts and end
// each port is associated with a specific process or server
// the server will be assigned to port 4000 via the "listen(4000)" method where the server will listen to any request that are sent to it, eventually communicating with our server

console.log('Server running at localhost:4000');